#!/bin/bash
#default mode
MODE='mock'
UPLOAD_TARBALLS='N'
#load default config
__DEFCONFIGS=./rpm_build_tools.conf

if [ -f "${__DEFCONFIGS}" ]
then
	source ${__DEFCONFIGS}
fi

#preparation 
#git config --global user.email "tekkamanv@163.com"
#git config --global user.name "TekkamanV"
#su anolis ; tmux a -s work
#tmux new -s work
#sudo yum install rpmdevtools tree qemu qemu-user-static mock-scm
#cp -a /mnt/nfs/ssh ~/.ssh

#cd /mnt/nfs/fedora-36-src-repo

upload_source_tarballs () 
{
	######为了保证编译顺利进行，不会因源码下载失败而出错######
	######以下操作仅为了提前下载好源码######
	git clone ${GIT_REPO_BASE}/${1}.git ${1}_WS/${1}
	pushd ${1}_WS/${1}
	git checkout ${DEVEL_REMOTE}/${DEVEL_BRANCH_ORG}
	git checkout -b ${DEVEL_BRANCH}
	fedpkg sources
	#若下载失败可尝试以下命令
	#spectool -g ${1}.spec
	#获取所有源码压缩文件的列表
	SRC_FILE=`cat sources | awk '{printf $2}' | sed -e 's/(//g' -e 's/)/ /g'`
	#强行加入git的commit中
	#并删除名为source的文件，确保编译系统不在编译阶段下载源码压缩包
	git add -f $SRC_FILE && git rm sources
	#本地提交带源码压缩包的commit
	git commit -am "$CMT_MESSAGE"
	#推到远程仓库，确保之后可使用这个带源码的提交点
	git push -f $DEVEL_REMOTE $DEVEL_BRANCH
	popd
	###### end ######
}

mock_chain_build ()
{
	echo "======== mock chain build start $MOCK_CONFIG========"
	#先构建SRPM,然后用‘--chain’构建RPMs，解决编译依赖问题。
	env G_SLICE=always-malloc \
	mock -r ${MOCK_CONFIG} \
	--scm-enable \
	--scm-option package=${1} \
	--scm-option branch=${DEVEL_BRANCH} \
	--buildsrpm \
	--resultdir ${1}_WS && \
	env G_SLICE=always-malloc \
	mock -r ${MOCK_CONFIG} \
	--localrepo ${LOCAL_REPO} \
	--recurse --chain \
	${1}_WS/${1}*src.rpm
	echo '======== mock chain build end ========'
}

mock_build ()
{
	echo "======== mock build start $MOCK_CONFIG========"
	#先构建SRPM,然后用mock build构建RPMs.
	env G_SLICE=always-malloc \
	mock -r ${MOCK_CONFIG} \
	--scm-enable \
	--scm-option package=${1} \
	--scm-option branch=${DEVEL_BRANCH} \
	--resultdir ${1}_WS
	echo '======== mock build end ========'
}

mock_clean ()
{
	env G_SLICE=always-malloc \
	mock -r ${MOCK_CONFIG} --cleanup-after --scrub=all
}
koji_build ()
{
	echo '======== koji build start ========'
	koji  add-pkg --owner=kojiadmin ${BUILD_TARGET} ${1};
	pushd ${1}_WS/${1}
	koji build --nowait ${BUILD_TARGET} \
	${GIT_REPO_BASE_HTTPS}/${1}.git#$(git rev-parse ${DEVEL_REMOTE}/${DEVEL_BRANCH})
	popd
	echo '======== koji build end ========'
}

help_msg ()
{
	echo '-M, mock chain build'
	echo '-m, mock build'
	echo '-C, clean'
	echo '-K, koji build'
	echo '-S, upload source tarballs'
	echo '-c <file.conf>, load config file'
	echo '-h, show this message'
}

#-C, mock clean
#-M, mock chain build
#-m, mock build
#-S, upload source tarballs
#-K, koji build
#-c <file.conf>, load config file

while getopts "CMmKSc:h" arg 
#选项后面的冒号表示该选项需要参数
do
    case $arg in
        c)
            echo getting config: $OPTARG
            source $OPTARG
            ;;
        C)
            mock_clean
            exit 0
            ;;
        M)
            MODE='chain'
            ;;
        m)
            MODE='mock'
            ;;
        K)
            MODE='koji'
            ;;
        S)
            UPLOAD_TARBALLS='Y'
            ;;
        h)  
            help_msg
            exit 0
            ;;
        *)  
		#当有不认识的选项的时候arg为?
            echo "unkonw argument"
            help_msg
            exit 1
	    ;;
    esac
#    echo $arg
done

shift $((OPTIND-1))

#1, create work dir
mkdir -p $LOCAL_REPO $WORK_DIR
pushd $WORK_DIR

PKG_NAME=${1-zlib}
# _WS : WorkSpace
mkdir -p ${PKG_NAME}_WS

if [ "${UPLOAD_TARBALLS}x" == "Yx" ]
then
	upload_source_tarballs ${PKG_NAME}
fi

if [ "${MODE}x" == "chainx" ]
then
#	echo "${MODE}x"
	mock_chain_build ${PKG_NAME}
else if [ "${MODE}x" == "mockx" ]
then
#	echo "${MODE}x"
	mock_build ${PKG_NAME}
else
#	echo "${MODE}y"
	koji_build ${PKG_NAME}
fi
fi

popd
