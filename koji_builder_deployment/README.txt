HOWTO

1, generate kojihub CA cert file, cp to certs/ dir(${KOJID_CA_CERT_PATH}) and check files' name which must be ${KOJID_CA_CERT_NAME}
2, generate koji builder pem file, cp to certs/ dir(${KOJID_PEMS_DIR}) and check files' name which must be ${KOJID_PEMS_PREFIX}_<builder number>.pem 

3, check koji_builder_deployment.conf, you don't have to modify most of parameters. BUT for "#for kojid config #####" you may need to set them for your koji system.

Note: KOJI_BUILDER_OS_IMAGE_URL is the koji builder base OS Image download link. *If you already have Image file, please put it in ${TMP_DIR} as file name ${KOJI_BUILDER_OS_IMAGE_MS} or ${KOJI_BUILDER_OS_IMAGE_TARBALL}.* The download will be skipped 

