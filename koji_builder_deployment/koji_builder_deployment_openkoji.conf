#!/usr/bin/env bash
DEPENDENCY_PKG="tmux koji-builder supervisor libvirt libvirt-client qemu qemu-img qemu-system-riscv stratisd stratis-cli zstd xz"
USER_LIST_CONF=${WORK_DIR}/user_list.conf
USER_KEY_DIR=${WORK_DIR}/user_key
#authorized_keys
#[type]_[arch]_[num].[subdomain].[domain]

BUILDER_TYPE=buildvm
BUILDER_ARCH=riscv64
SERVER_SUB_DN=openkoji
SERVER_DN=iscas.ac.cn

KOJI_BUILDER_MASTER_TAPE=
#BUILDER_FIX=N
BUILDER_NUM_WIDTH=3
BUILDER_STEP=1

BUILDER_START_NUM=1
BUILDER_END_NUM=3

#dir structure of sdk
WORK_DIR=$(pwd)
TEMPLATE_DIR=${WORK_DIR}/templates
TMP_DIR=${WORK_DIR}/.deployment
TMP_DATA_DIR=${TMP_DIR}/data
#STORAGE_POOL=/var/lib/libvirt/images
STORAGE_POOL=/var/lib/libvirt/images

BUILDER_HOSTS='192.168.6.177   openkoji.iscas.ac.cn'

##################################
#for koji builder xml template####
#@@opensbi_bin@@
OPENSBI_PATH=/var/lib/libvirt/images/fw_dynamic.bin
#@@uboot_bin@@
UBOOT_PATH=/var/lib/libvirt/images/u-boot.bin

#@@builder_name@@
BUILDER_NAME_PREFIX=${BUILDER_TYPE}_${BUILDER_ARCH}
#@@image_builder_memory@@
BUILDER_MEMORY=25165824
#@@image_builder_OS@@
BUILDER_OS_PREFIX=${BUILDER_ARCH}
#@@image_builder_data@@
BUILDER_DATA_PREFIX=${BUILDER_ARCH}
##################################

#prefix is ${TEMPLATE_DIR}
#KOJID_DIR=kojid
#absolute path, will be copied to ${TMP_DIR}
KOJID_CA_CERT_NAME=koji_ca_cert.crt
KOJID_CA_CERT_PATH=${WORK_DIR}/../certs/${KOJID_CA_CERT_NAME}
#absolute path
KOJID_PEMS_DIR=${WORK_DIR}/../certs
KOJID_PEMS_PREFIX=${BUILDER_TYPE}_${BUILDER_ARCH}_
KOJID_PEMS_SUBFIX=.${SERVER_SUB_DN}.${SERVER_DN}
#subfix will be .pem, so name in spems dir should be
# ${KOJID_PEMS_PREFIX}_xxx.pem


#for kojid config #####
#for riscv64 qemu, 2 is best
KOJID_MAXJOBS=2
#48G for some big software
KOJID_MINSPACE=49152
#10 days, gcc need more time > 150 hours
KOJID_RPMBUILD_TIMEOUT=864000
KOJID_SERVER='http://openkoji.iscas.ac.cn/kojihub'
KOJID_TOPURL='http://openkoji.iscas.ac.cn/kojifiles'
KOJID_ALLOWED_SCMS='gitee.com:src-oepkgs-fedora-rv/*:no:true *.fedoraproject.org:*:no:fedpkg,sources fedora.riscv.rocks:*:no:fedpkg,sources *:*:no:fedpkg,sources'
KOJID_SMTPHOST='openkoji.iscas.ac.cn'
KOJID_FROM_ADDR='Koji Build System <kojiadmin@openkoji.iscas.ac.cn>'
KOJID_CERT="/etc/kojid/client.crt"
KOJID_SERVERCA="/etc/kojid/serverca.crt"
##################################
KOJI_BUILDER_OS_IMAGE_DECOMPRESS='xz --format=auto'
#KOJI_BUILDER_OS_IMAGE_DECOMPRESS='zstd'

KOJI_BUILDER_XML_TEMPLATE=${TEMPLATE_DIR}/${BUILDER_ARCH}_builder_template.xml

KOJI_BUILDER_OS_IMAGE_URL=https://openkoji.iscas.ac.cn/pub/dl/riscv/qemu/koji_builder/fedora-riscv64-image-builder-latest.raw.xz
KOJI_BUILDER_FW_URL=https://openkoji.iscas.ac.cn/pub/dl/riscv/qemu/koji_builder/fw_dynamic.bin
KOJI_BUILDER_UBOOT_URL=https://openkoji.iscas.ac.cn/pub/dl/riscv/qemu/koji_builder/u-boot.bin
#by this name, we can make sure that
#the image will be decompressed to ${BUILDER_NAME_PREFIX}.raw
KOJI_BUILDER_IMAGE_PREFIX=${BUILDER_OS_PREFIX}
KOJI_BUILDER_OS_IMAGE_TARBALL=${KOJI_BUILDER_IMAGE_PREFIX}.raw.xz
KOJI_BUILDER_OS_IMAGE_MS=${KOJI_BUILDER_IMAGE_PREFIX}.raw

KOJI_BUILDER_DATA_FORMAT=raw
KOJI_BUILDER_DATA_MS=${KOJI_BUILDER_IMAGE_PREFIX}.data.${KOJI_BUILDER_DATA_FORMAT}
KOJI_BUILDER_DATA_SIZE=80G
KOJI_BUILDER_DATA_PART1_START=4096
KOJI_BUILDER_DATA_PART1_END=0
KOJI_BUILDER_DATA_PART1_NAME=kb_data
#KOJI_BUILDER_DATA_PART1_TYPE=EBD0A0A2-B9E5-4433-87C0-68B6B72699C7
KOJI_BUILDER_DATA_PART1_TYPE=0FC63DAF-8483-4772-8E79-3D69D8477DE4
#you may need to change this for your env
KOJI_BUILDER_DATA_NDB_DEV=/dev/nbd4
KOJI_BUILDER_DATA_MNT_DIR=${TMP_DIR}/mnt
